const path = require("path");

module.exports = {
  home: (req, res) => {
    res.sendFile(path.join(__dirname, "../html/index.html"));
  },
  game: (req, res) => {
    res.sendFile(path.join(__dirname, "../game/index.html"));
  },

  //   home: (req, res) => {
  //     res.sendFile(path.join(__dirname, "../html/index.html"));
  //   },

  //   product: (res, req) => {
  //     return res.json({
  //       id: 1,
  //       name: "Buku Cerita",
  //       price: 15000,
  //     });
  //   },

  login: (req, res) => {
    const dummyUser = {
      email: "iqbal@gmail.com",
      password: "iqbal",
    };

    if (req.body.email === dummyUser.email && req.body.password === dummyUser.password) {
      return res.json(
        {
          message: "Login Berhasil",
          data: dummyUser,
        },
        200
      );
    }

    return res.json(
      {
        message: "Password atau Email Salah",
      },
      400
    );
  },
};
