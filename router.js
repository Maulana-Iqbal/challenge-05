const express = require("express");
const router = express.Router();
const examplecontroller = require("./controllers/examplecontroller");

router.get("/", examplecontroller.home);
router.get("/game", examplecontroller.game);
router.post("/login", examplecontroller.login);

router.get("/bio", (req, res) => {
  res.send("Iqbal Maulana!");
});

//apabila endpointnya dinamis sesuai ID
// router.get("/product/:id", (req, res) => {
//   const id = req.params.id;

//   return res.send(`id is: ${id}`);
// });

// router.get("/product/:id", examplecontroller.getProductById);

module.exports = router;
